//
//  ViewController.swift
//  LabProject
//
//  Created by Wi-Fai on 03.02.2023.
//

import UIKit

 final class MainViewWithTableView: UIViewController {
     
     static let resourceString = "https://newsapi.org/v2/top-headlines?country=us&apiKey=c24ea739275f41e4b3630764cd0588b5&pageSize=20"
     let tableView = UITableView()
     let freshNews = UILabel()

     private let newsAPIService = NewsAPIService()
     
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .gray
        
        newsAPIService.getData(from: Self.resourceString) { [weak self] _ in
           self?.tableView.reloadData()
        }
        
        self.addOnView()
        self.freshNewsConfig()
        self.tableViewConfig()
    }
    
     
     @objc private func didPullRefresh() {         
         DispatchQueue.main.async {
             self.tableView.refreshControl?.endRefreshing()
             self.tableView.reloadData()
         }
     }
     
     override func viewDidDisappear(_ animated: Bool) {
         SaveToMemory().saveToMemory(array: lastNews)
     }
 }

extension MainViewWithTableView: UITableViewDelegate, UITableViewDataSource {
    //MARK: freshNews label config
    
    private func freshNewsConfig() {
        freshNews.text = "NEWS"
        freshNews.textAlignment = .center
        
        NSLayoutConstraint.activate([
            freshNews.topAnchor.constraint(equalTo: view.topAnchor, constant: 50),
            freshNews.leadingAnchor.constraint(equalTo: view.leadingAnchor,constant: 0),
            freshNews.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            freshNews.heightAnchor.constraint(equalToConstant: 50)
        ])
    }
    
    //MARK: TableView config
    private func addOnView() {
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(freshNews)
        freshNews.translatesAutoresizingMaskIntoConstraints = false
      }
      
       private  func tableViewConfig() {
           tableView.delegate = self
           tableView.dataSource = self
           tableView.register(NewsCell.self, forCellReuseIdentifier: NewsCell.identifier)

           tableView.backgroundColor = .gray
           tableView.rowHeight = 150
           tableView.refreshControl = UIRefreshControl()
           tableView.refreshControl?.addTarget(self,
                                               action: #selector(didPullRefresh),
                                               for: .valueChanged)
           
           NSLayoutConstraint.activate([
               tableView.topAnchor.constraint(equalTo: freshNews.bottomAnchor, constant: 5),
               tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
               tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
               tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
           ])
       }

    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        lastNews.count
    }
            
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NewsCell.identifier) as! NewsCell
        
        cell.label.text = lastNews[indexPath.row].title
        cell.countLabel.text = "Views \(lastNews[indexPath.row].countOfView)"
        getImage(url: lastNews[indexPath.row].urlToImage) {
            cell.newsImage.image = $0
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        lastNews[indexPath.row].increaseOne()

        let describeView = SummaryView()
        describeView.titleLabel.text = lastNews[indexPath.row].title
        describeView.content.text = lastNews[indexPath.row].content
        
        getImage(url: lastNews[indexPath.row].urlToImage) {
            describeView.imageOfNews.image = $0
        }
        
        describeView.dateLabel.text = lastNews[indexPath.row].publishedAt
        describeView.source.text = lastNews[indexPath.row].sourceName
        describeView.sourceUrl = lastNews[indexPath.row].sourceUrl

        present(describeView, animated: true)
        let indexPath = IndexPath.init(row: indexPath.row, section: 0)
        tableView.reloadRows(at: [indexPath], with: .fade)
    }
    
    private func getImage(url:String, completion: @escaping (UIImage?) -> Void) {
        guard let url = URL(string:url) else { return }
        
        DispatchQueue.global().async {
            if let data = try? Data(contentsOf: url) {
                DispatchQueue.main.async{
                    completion(UIImage(data: data))
                }
            }
        }
    }
}


