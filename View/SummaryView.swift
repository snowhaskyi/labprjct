//
//  summaryView.swift
//  LabProject
//
//  Created by Wi-Fai on 04.02.2023.
//

import UIKit

final class SummaryView: UIViewController {
    
    //MARK: Views
    var titleLabel = UILabel()
    var imageOfNews = UIImageView()
    var content = UITextView()
    var viewBar = UIView()
    
    var sourceUrl: String? = nil
    var dateLabel = UILabel()
    var source = UILabel()
    var urlToSource = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .cyan
        
        addOnView()
        titleLabelConfig()
        imageOfNewsConfig()
        contentConfig()
        viewBarConfig()
    }

    private func addOnView() {
        let  itemArray = [ imageOfNews, titleLabel, content, viewBar]
        for item in itemArray {
            view.addSubview(item)
            item.translatesAutoresizingMaskIntoConstraints = false
        }
    }
    
    private  func addOnViewBar() {
       let  itemArray = [ dateLabel, source, urlToSource]
       for item in itemArray {
           viewBar.addSubview(item)
           item.translatesAutoresizingMaskIntoConstraints = false
       }
   }
}

private extension SummaryView {
    //MARK: titleLabel config
    func titleLabelConfig() {
        titleLabel.font = .systemFont(ofSize: 25)
        titleLabel.textAlignment = .center
        titleLabel.textColor = .black
        titleLabel.numberOfLines = 0
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 25),
            titleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 25),
            titleLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -25),
        ])
    }
    //MARK: imageOfNews config
    func imageOfNewsConfig() {
        imageOfNews.backgroundColor = .gray
        imageOfNews.contentMode = .scaleAspectFill
        
        NSLayoutConstraint.activate([
            imageOfNews.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 10),
            imageOfNews.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            imageOfNews.widthAnchor.constraint(equalToConstant: 200),
            imageOfNews.heightAnchor.constraint(equalToConstant: 150),
        ])
    }
    
    //MARK: content config
    func contentConfig() {
        content.textAlignment = .left
        content.backgroundColor = .clear
        content.textColor = .black
        content.isEditable = false
        content.font = .systemFont(ofSize: 30)
        content.textContainer.lineBreakMode = .byWordWrapping
        
        let margin:CGFloat = 10
        NSLayoutConstraint.activate([
            content.topAnchor.constraint(equalTo: imageOfNews.bottomAnchor, constant: margin),
            content.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: margin),
            content.trailingAnchor.constraint(equalTo: view.trailingAnchor,constant: -margin),
            content.bottomAnchor.constraint(equalTo: viewBar.topAnchor, constant: -margin),
        ])
    }
    //MARK: viewBar config
    func viewBarConfig() {
        addOnViewBar()
        urlToSourceConfig()
        
        viewBar.backgroundColor = .clear
        viewBar.layer.borderColor = UIColor.black.cgColor
        viewBar.layer.borderWidth = 3
        viewBar.layer.cornerRadius = 20
        viewBar.clipsToBounds = true
        
        labelStandart(label: dateLabel)
        labelStandart(label: source)
        
        let margin:CGFloat = 10
        NSLayoutConstraint.activate([
            viewBar.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: margin),
            viewBar.trailingAnchor.constraint(equalTo: view.trailingAnchor,constant: -margin),
            viewBar.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -(margin + 5)),
            viewBar.heightAnchor.constraint(equalToConstant: 50),
                        
            dateLabel.topAnchor.constraint(equalTo: viewBar.topAnchor, constant: 0),
            dateLabel.leadingAnchor.constraint(equalTo: viewBar.leadingAnchor, constant: 0),
            dateLabel.bottomAnchor.constraint(equalTo: viewBar.bottomAnchor, constant: 0),
            dateLabel.widthAnchor.constraint(equalToConstant: (view.frame.width - margin * 2) / 3),
            
            source.topAnchor.constraint(equalTo: viewBar.topAnchor, constant: 0),
            source.leadingAnchor.constraint(equalTo: dateLabel.trailingAnchor, constant: 0),
            source.bottomAnchor.constraint(equalTo: viewBar.bottomAnchor, constant: 0),
            source.widthAnchor.constraint(equalToConstant: (view.frame.width - margin * 2) / 3),
        ])
        
    }
    
    //MARK: urlToSource config
    func urlToSourceConfig() {
        urlToSource.setTitle("source -->", for: .normal)
        urlToSource.backgroundColor = .green
        urlToSource.addTarget(self, action: #selector(goByUrl), for: .touchUpInside)

        let margin:CGFloat = 10
        NSLayoutConstraint.activate([
            urlToSource.topAnchor.constraint(equalTo: viewBar.topAnchor, constant: 0),
            urlToSource.leadingAnchor.constraint(equalTo: source.trailingAnchor, constant: 0),
            urlToSource.bottomAnchor.constraint(equalTo: viewBar.bottomAnchor, constant: 0),
            urlToSource.widthAnchor.constraint(equalToConstant: (view.frame.width - margin * 2) / 3),
        ])
    }
    
    func labelStandart(label: UILabel) {
        label.textAlignment = .center
        label.backgroundColor = .systemPink
        label.textColor = .black
        label.numberOfLines = 0
    }

    @objc private func goByUrl() {
        guard let url = URL(string: sourceUrl ?? "https://www.google.com") else {
            return
        }
        let newVC = WebViewController(url: url)
        present(newVC, animated: true)
    }
    
}
