//
//  NewsCell.swift
//  LabProject
//
//  Created by Wi-Fai on 03.04.2023.
//

import UIKit

final class NewsCell: UITableViewCell {
    
    static let identifier = "NewsCellIdentifier"

    var newsImage = UIImageView()
    let label = UILabel()
    let countLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?){
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.backgroundColor = .gray
        
        let showOnVC = [newsImage, label, countLabel ]
        for i in showOnVC {
            contentView.addSubview(i)
            i.translatesAutoresizingMaskIntoConstraints = false
        }
        
        labelConfig()
        imageViewConfig()
        countLabelConfig()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension NewsCell {
    //MARK: newsImage config
    private func imageViewConfig() {
        newsImage.layer.borderWidth = 3
        newsImage.layer.borderColor = UIColor.black.cgColor
        newsImage.layer.cornerRadius = 20
        newsImage.clipsToBounds = true
        newsImage.contentMode = .scaleAspectFill

        NSLayoutConstraint.activate([
            newsImage.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0),
            newsImage.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0),
            newsImage.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10),
            newsImage.widthAnchor.constraint(equalToConstant: contentView.frame.width / 2.5)
        ])
    }
    
    //MARK: label config
    private func labelConfig() {
        label.textAlignment = .center
        label.text = "test"
        label.numberOfLines = 0
        label.textColor = .black
        
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0),
            label.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0),
            label.leadingAnchor.constraint(equalTo: newsImage.trailingAnchor, constant:  10),
            label.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20)
        ])
    }
    
    //MARK: countLabel config
    
    private func countLabelConfig() {
        countLabel.textColor = .green
        countLabel.textAlignment = .center
        
        NSLayoutConstraint.activate([
            countLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor,constant: -10),
            countLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor,constant: -10),
            countLabel.heightAnchor.constraint(equalToConstant: contentView.frame.height / 3)
        ])
    }
}
