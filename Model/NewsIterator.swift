//
//  NewsIterator.swift
//  LabProject
//
//  Created by Wi-Fai on 04.02.2023.
//

struct NewsRequestResult: Codable {
    var status: String
    var totalResults: Int?
    let articles: [Article]
}

struct Source: Codable {
    var id: String?
    var name: String?
}

struct Article: Codable {
    var source: Source?
    var author: String?
    var title: String?
    var description: String?
    var url: String?
    var urlToImage: String?
    var publishedAt: String?
    var content: String?
}
