//
//  DataSource.swift
//  LabProject
//
//  Created by Wi-Fai on 05.02.2023.
//

import UIKit

let folderName = "LastNewsForLab"
let fileName = "LastNewsFile"

var lastNews: [FillData] = []

var arrayData: [Article] = []
var arrayForWork: [FillData] = []

struct FillData: Codable {
    
    var title: String
    var urlToImage: String
    var description: String
    var publishedAt: String
    var sourceUrl: String
    var sourceName: String
    var author: String
    var content: String
    var countOfView: Int
    
    init(title: String, urlToImage: String, description: String, publishedAt: String, sourceUrl: String, sourceName: String, countOfView: Int, content:String, author:String) {
        self.title = title
        self.urlToImage = urlToImage
        self.description = description
        self.publishedAt = publishedAt
        self.sourceUrl = sourceUrl
        self.sourceName = sourceName
        self.countOfView = countOfView
        self.content = content
        self.author = author
    }
    
    mutating func increaseOne() {
            countOfView = countOfView + 1
        }
}

