//
//  GetData.swift
//  LabProject
//
//  Created by Wi-Fai on 05.02.2023.
//

import UIKit

final class NewsAPIService {
    
    func cashNews(_ news: NewsRequestResult) {
        arrayData = news.articles

        for item in arrayData {
            let news = FillData(title: item.title ?? "",
                                urlToImage: item.urlToImage ?? "",
                                description: item.description ?? "",
                                publishedAt: item.publishedAt ?? "",
                                sourceUrl: item.url ?? "",
                                sourceName: item.source?.name ?? "",
                                countOfView: 0,
                                content: item.content ?? "",
                                author: item.author ?? "")
            arrayForWork.append(news)
        }
        
        
        lastNews = arrayForWork
        SaveToMemory().saveToMemory(array: lastNews)
    }
    
    func getData(from url: String, completion: @escaping (Result<NewsRequestResult, Error>) -> Void) {
        let task = URLSession.shared.dataTask(with: URL(string: url)!, completionHandler: { [weak self] data, response, error in
            guard let data = data, error == nil else {
                print ("Something wrong")
                return
            }
            
            do {
                print("Try to get")
                let result = try JSONDecoder().decode(NewsRequestResult.self, from: data)
                self?.cashNews(result)
                
                print("Success")
                DispatchQueue.main.async {
                    completion(.success(result))
                }
            }
            catch {
                print(error)
                DispatchQueue.main.async {
                    completion(.failure(error))
                }
            }
        })
        task.resume()
    }
}
