//
//  SaveToMemory.swift
//  LabProject
//
//  Created by Wi-Fai on 05.02.2023.
//

import UIKit

final class SaveToMemory {
    
    func saveToMemory(array:[FillData]) {
        var lastNewsArray: Data?
        let manager = FileManager.default
        guard let url = manager.urls(for: .documentDirectory,
                                     in: .userDomainMask).first else {return}
        
        let folderUrl = url.appendingPathComponent(folderName)
        
        do {
            // создаем папку по ссылке
            try manager.createDirectory(at: folderUrl,
                                        withIntermediateDirectories: true)
            //print(folderUrl)
        } catch {
            print(error)
        }
        
        
        do {
            lastNewsArray = try JSONEncoder().encode(array)
        } catch {
            print(error.localizedDescription)
        }
        
        let fileUrl = folderUrl.appendingPathComponent( fileName + ".json")
        manager.createFile(atPath: fileUrl.path,
                           contents: lastNewsArray)
    }
    
    func restoreGoals() {
        let manager = FileManager.default
        
        guard let url = manager.urls(for: .documentDirectory,
                                     in: .userDomainMask).first else {return}
        let folderURL = url.appendingPathComponent(folderName)
        let fileURL = folderURL.appendingPathComponent( fileName + ".json")
        
        do {
            let data = try Data(contentsOf: fileURL)
            lastNews = try JSONDecoder().decode( [FillData].self, from: data )
        } catch {
            print(error)
        }
        
    }
}
